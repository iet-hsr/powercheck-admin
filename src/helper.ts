export function assertUnreachable(__: never): never {
  throw new Error('Didn\'t expect to get here');
}
